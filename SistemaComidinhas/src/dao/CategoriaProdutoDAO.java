/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import factory.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import modelo.CategoriaProduto;

/**
 *
 * @author Arthur
 */
public class CategoriaProdutoDAO {
    private Connection connection;
    String categoriaprodNome;
    String categoriaprodDescricao;
    
    public CategoriaProdutoDAO(){
        this.connection = new ConnectionFactory().getConnection();
    }
    public void adiciona(CategoriaProduto categoriaproduto) throws SQLException{
        String sql = "INSERT INTO CategoriaProduto(categoriaprodNome, categoriaprodDescricao) VALUES(?,?)";
        try{
            try (PreparedStatement stmt = connection.prepareStatement(sql)) {
                stmt.setString(1, categoriaproduto.getCategoriaprodNome());
                stmt.setString(2, categoriaproduto.getCategoriaprodDescricao());
                stmt.execute();
                stmt.close();
            }
        }
        catch (SQLException u) { 
            throw new RuntimeException(u);
        }             
    }

}
