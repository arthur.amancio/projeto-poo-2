/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.daoimplementation;

import db.DAOFactory;
import db.daointerface.AlunoDAO;
import db.model.TbAluno;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author admin
 */
public class AlunoDAOImpl implements AlunoDAO {

    @Override
    public boolean delteAluno(TbAluno a) {
        try {
            Connection con = DAOFactory.getDatabaseConnection();
            PreparedStatement ps = con.prepareStatement("DELETE FROM sc_universidade.tb_aluno WHERE nr_matricula = ?");
            ps.setInt(1, a.getNrMatricula());
            ps.execute();
            ps.close();
            con.close();
            return true;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(AlunoDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public TbAluno selecionarAluno(TbAluno a) {
        try {
            Connection con = DAOFactory.getDatabaseConnection();
            //PENDÊNCIA : fazer um filtro mais elaborado com os dados de Aluno
            String strSQL = "SELECT * FROM sc_universidade.tb_aluno WHERE nr_matricula = ?";
            PreparedStatement ps = con.prepareStatement(strSQL);
            ps.setInt(1, a.getNrMatricula());
            ResultSet rs = ps.executeQuery();
            TbAluno alunoSelecionado = new TbAluno();
            if (rs.next()) {
                
                alunoSelecionado.setNrCpf(rs.getString("nr_cpf"));
                alunoSelecionado.setNmAluno(rs.getString("nm_aluno"));
                alunoSelecionado.setNmEmail(rs.getString("nm_email"));
                alunoSelecionado.setNrMatricula(rs.getInt("nr_matricula"));
                alunoSelecionado.setNrTelefone(rs.getString("nr_telefone"));
            }
            ps.close();
            con.close();
            return alunoSelecionado;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(AlunoDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    @Override
    public List<TbAluno> selecionarAlunos(TbAluno a) {
        try {
            List<TbAluno> listaAlunos = new ArrayList<TbAluno>();
            Connection con = DAOFactory.getDatabaseConnection();
            //PENDÊNCIA : fazer um filtro mais elaborado com os dados de Aluno
            String strSQL = "SELECT * FROM sc_universidade.tb_aluno ";
            if (a != null && a.getNmAluno() != null) {
                strSQL += "WHERE nm_aluno ilike '%?%' ";
            }

            PreparedStatement ps = con.prepareStatement(strSQL);
            if (a != null && a.getNmAluno() != null) {
                ps.setString(1, a.getNmAluno());
            }

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                TbAluno alunoSelecionado = new TbAluno();
                alunoSelecionado.setNrCpf(rs.getString("nr_cpf"));
                alunoSelecionado.setNmAluno(rs.getString("nm_aluno"));
                alunoSelecionado.setNmEmail(rs.getString("nm_email"));
                alunoSelecionado.setNrMatricula(rs.getInt("nr_matricula"));
                alunoSelecionado.setNrTelefone(rs.getString("nr_telefone"));
                listaAlunos.add(alunoSelecionado);
            }
            ps.close();
            con.close();
            return listaAlunos;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(AlunoDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    @Override
    public TbAluno inserirAluno(TbAluno a) {
        try {
            Connection con = DAOFactory.getDatabaseConnection();
            PreparedStatement ps = con.prepareStatement("INSERT INTO sc_universidade.tb_ALuno (nm_aluno, nm_email, nr_cpf, nr_telefone) values (?, ?, ?, ?) returning nr_matricula");
            //ps.setInt(1, a.getNrMatricula());
            ps.setString(1, a.getNmAluno());
            ps.setString(2, a.getNmEmail());
            ps.setString(3, a.getNrCpf());
            ps.setString(4, a.getNrTelefone());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                a.setNrMatricula(rs.getInt(1));
            }
            ps.close();
            con.close();
            return a;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(AlunoDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public boolean modificarAluno(TbAluno a) {
        try {
            Connection con = DAOFactory.getDatabaseConnection();
            PreparedStatement ps = con.prepareStatement("UPDATE sc_universidade.tb_ALuno set nm_aluno =? , nm_email = ?, nr_cpf = ?, nr_telefone = ? WHERE nr_matricula = ? ");
            ps.setInt(5, a.getNrMatricula());
            ps.setString(1, a.getNmAluno());
            ps.setString(2, a.getNmEmail());
            ps.setString(3, a.getNrCpf());
            ps.setString(4, a.getNrTelefone());
            ps.execute();
            ps.close();
            con.close();
            return true;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(AlunoDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

}
