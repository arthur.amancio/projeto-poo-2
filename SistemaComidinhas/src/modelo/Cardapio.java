/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Arthur
 */
public class Cardapio {
    int cardapioId;
    String cardapioPratoDeEntrada;
    String cardapioRefeição;
    String cardapioSobremesa;
    int Produto_produtoId;

    public int getCardapioId() {
        return cardapioId;
    }

    public void setCardapioId(int cardapioId) {
        this.cardapioId = cardapioId;
    }

    public String getCardapioPratoDeEntrada() {
        return cardapioPratoDeEntrada;
    }

    public void setCardapioPratoDeEntrada(String cardapioPratoDeEntrada) {
        this.cardapioPratoDeEntrada = cardapioPratoDeEntrada;
    }

    public String getCardapioRefeição() {
        return cardapioRefeição;
    }

    public void setCardapioRefeição(String cardapioRefeição) {
        this.cardapioRefeição = cardapioRefeição;
    }

    public String getCardapioSobremesa() {
        return cardapioSobremesa;
    }

    public void setCardapioSobremesa(String cardapioSobremesa) {
        this.cardapioSobremesa = cardapioSobremesa;
    }

    public int getProduto_produtoId() {
        return Produto_produtoId;
    }

    public void setProduto_produtoId(int Produto_produtoId) {
        this.Produto_produtoId = Produto_produtoId;
    }
}
