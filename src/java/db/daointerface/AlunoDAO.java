/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.daointerface;

import db.model.TbAluno;
import java.util.List;

/**
 *
 * @author admin
 */
public interface AlunoDAO {
    /*
    INSERIR
    DELETE
    MODIFICAR
    MOSTRAR TODOS
    MOSTRAR UM
    */
    public boolean delteAluno(TbAluno a);
    public TbAluno selecionarAluno(TbAluno a);
    public List<TbAluno> selecionarAlunos(TbAluno a);
    public TbAluno inserirAluno(TbAluno a);
    public boolean modificarAluno(TbAluno a);
}
